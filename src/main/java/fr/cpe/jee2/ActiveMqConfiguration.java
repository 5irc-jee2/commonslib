package fr.cpe.jee2;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource({"classpath:activemq.properties"})
public class ActiveMqConfiguration {
}
