package fr.cpe.jee2.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class EnveloppeDTO implements Serializable {
    private int uuid;
    private String methodName;

    private Map<Integer,Object> parameters;

    public int getUuid() {
        return uuid;
    }

    public void setUuid(int uuid) {
        this.uuid = uuid;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Map<Integer, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<Integer, Object> parameters) {
        this.parameters = parameters;
    }

    public void addParameters(Integer idParm, Object param){
        if(null == parameters){
            parameters = new HashMap<Integer,Object>();
        }
        parameters.put(idParm, param);
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("EnveloppeDTO{");
        stringBuilder.append("uuid=").append(uuid);
        stringBuilder.append(", methodName='").append(methodName).append('\'');
        stringBuilder.append(", parameters=").append(parameters);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
