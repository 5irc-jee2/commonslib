package fr.cpe.jee2.emitter.controller;

import fr.cpe.jee2.dto.EnveloppeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class BusService {

    @Autowired
    JmsTemplate jmsTemplate;

    public void sendMsg(EnveloppeDTO enveloppeDTO, String busName) {
        System.out.println("[BUSSERVICE] SEND String MSG=[" + enveloppeDTO.toString()
                + "] to Bus=[" + busName + "]");
        jmsTemplate.convertAndSend(busName, enveloppeDTO);
    }
}