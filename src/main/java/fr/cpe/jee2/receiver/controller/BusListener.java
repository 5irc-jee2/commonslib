package fr.cpe.jee2.receiver.controller;

import fr.cpe.jee2.dto.EnveloppeDTO;
import org.apache.activemq.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public abstract class BusListener {

    @Autowired
    JmsTemplate jmsTemplate;

    @JmsListener(destination = "${spring.queue.name}")
    public void receiveMessage(EnveloppeDTO pEnveloppeDTO, Message message) {
        System.out.println("[BUSLISTENER] RECEIVED String MSG=[" + pEnveloppeDTO.toString() + "]");
        this.parseData(pEnveloppeDTO);
    }

    public abstract void parseData(final EnveloppeDTO pEnveloppeDTO);
}